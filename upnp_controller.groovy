/**
 *  UPnP Media Player
 *
 *  Copyright 2014 Josh Bohde
 *
 */
metadata {
  definition (name: "UPnP Media Player", namespace: "joshbohde", author: "Josh Bohde") {
    capability "Refresh"
    capability "Polling"
    capability "Music Player"
  }

  simulator {
    // TODO: define status and reply messages here
  }

  tiles {

	standardTile("main", "device.status", width: 1, height: 1, canChangeIcon: true) {
      state "paused", label:'Paused', action:"music Player.play", icon:"st.Electronics.electronics16", nextState:"playing", backgroundColor:"#ffffff"
      state "playing", label:'Playing', action:"music Player.pause", icon:"st.Electronics.electronics16", nextState:"paused", backgroundColor:"#79b821"
	}

	// Row 1
	standardTile("nextTrack", "device.status", width: 1, height: 1, decoration: "flat") {
      state "next", label:'', action:"music Player.nextTrack", icon:"st.sonos.next-btn", backgroundColor:"#ffffff"
	}
	standardTile("play", "device.status", width: 1, height: 1, decoration: "flat") {
      state "default", label:'', action:"music Player.play", icon:"st.sonos.play-btn", nextState:"playing", backgroundColor:"#ffffff"
      state "grouped", label:'', action:"music Player.play", icon:"st.sonos.play-btn", backgroundColor:"#ffffff"
	}
	standardTile("previousTrack", "device.status", width: 1, height: 1, decoration: "flat") {
      state "previous", label:'', action:"music Player.previousTrack", icon:"st.sonos.previous-btn", backgroundColor:"#ffffff"
	}

    standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
      state "default", action:"refresh.refresh", icon: "st.secondary.refresh"
    }

	// Row 2
	standardTile("status", "device.status", width: 1, height: 1, decoration: "flat", canChangeIcon: true) {
      state "playing", label:'Playing', action:"music Player.pause", icon:"st.Electronics.electronics16", nextState:"paused", backgroundColor:"#ffffff"
      state "stopped", label:'Stopped', action:"music Player.play", icon:"st.Electronics.electronics16", nextState:"playing", backgroundColor:"#ffffff"
      state "paused", label:'Paused', action:"music Player.play", icon:"st.Electronics.electronics16", nextState:"playing", backgroundColor:"#ffffff"
	}
	standardTile("pause", "device.status", width: 1, height: 1, decoration: "flat") {
      state "default", label:'', action:"music Player.pause", icon:"st.sonos.pause-btn", nextState:"paused", backgroundColor:"#ffffff"
      state "grouped", label:'', action:"music Player.pause", icon:"st.sonos.pause-btn", backgroundColor:"#ffffff"
	}
	standardTile("mute", "device.mute", inactiveLabel: false, decoration: "flat") {
      state "unmuted", label:"", action:"music Player.mute", icon:"st.custom.sonos.unmuted", backgroundColor:"#ffffff", nextState:"muted"
      state "muted", label:"", action:"music Player.unmute", icon:"st.custom.sonos.muted", backgroundColor:"#ffffff", nextState:"unmuted"
	}

	// Row 3
	controlTile("levelSliderControl", "device.level", "slider", height: 1, width: 3, inactiveLabel: false) {
      state "level", action:"music Player.setLevel", backgroundColor:"#ffffff"
	}


    main "main"
    details(["previousTrack", "play", "nextTrack",
             "status","pause","mute",
             "levelSliderControl",
             "refresh"])
  }
}

def initChild(d){
  state.services = d
}

// handle commands
def poll() {
  log.debug "Executing 'poll'"
  refresh()
}

def refresh() {
  log.debug "Executing 'refresh'"
  def result = subscribe()
  result << services()
  result << getStatus()
  result << getVolume()
  result << getMute()
  // Ingore nulls, in case service data is broken
  result.flatten().findAll { it }
}

def play() {
  log.debug "Executing 'play'"
  soap("Play", ["InstanceId": "0", "Speed": 1])
}

def pause() {
  log.debug "Executing 'pause'"
  soap("Pause", ["InstanceId": "0"])
}

def stop() {
  log.debug "Executing 'stop'"
  soap("Stop", ["InstanceId": "0"])
}

def nextTrack() {
  log.debug "Executing 'nextTrack'"
  soap("Next", ["InstanceId": "0"])
}

def playTrack() {
  log.debug "Executing 'playTrack'"
  // TODO: handle 'playTrack' command
}

def setLevel(val) {
  log.debug "Executing 'setLevel($val)'"
  soap("SetVolume", ["InstanceId": "0", "Channel": "Master", "DesiredVolume": val], "RenderingControl")
}

def playText() {
  log.debug "Executing 'playText'"
  // TODO: handle 'playText' command
}

def mute() {
  log.debug "Executing 'mute'"
  soap("SetMute", ["InstanceId": "0", "Channel": "Master", "DesiredMute": 1], "RenderingControl")
}

def getVolume()
{
  log.debug "getVolume()"
  soap("GetVolume", [InstanceID: 0, Channel: "Master"], "RenderingControl")
}

def getMute()
{
  log.debug "getMute()"
  soap("GetMute", [InstanceID: 0, Channel: "Master"], "RenderingControl")
}


def previousTrack() {
  log.debug "Executing 'previousTrack'"
  soap("Previous", ["InstanceId": "0"])
}

def unmute() {
  log.debug "Executing 'unmute'"
  soap("SetMute", ["InstanceId": "0", "Channel": "Master", "DesiredMute": 0], "RenderingControl")
}

def setTrack() {
  log.debug "Executing 'setTrack'"
  // TODO: handle 'setTrack' command
}

def resumeTrack() {
  log.debug "Executing 'resumeTrack'"
  // TODO: handle 'resumeTrack' command
}

def restoreTrack() {
  log.debug "Executing 'restoreTrack'"
  // TODO: handle 'restoreTrack' command
}

def getStatus(){
  soap("GetTransportInfo", ["InstanceId": "0"])
}

def parse(description){
  log.debug "Parsing '${description}'"
  def m = parseLanMessage(description)

  // Parse Subscription confirmation
  if(m.headers && !m.body){
    def sid = parseSid(m.headers)
    if (sid) {
      updateSid(sid)
    }
  }

  if(m.xml){
    parseEvents(m.xml.property.LastChange)

    def services = parseServices(m.xml.device)
    if(services?.id){
      def s = state.services
      s.services  = services.services
      state.services = s
    }

    updateLevel(m.xml.Body.GetVolumeResponse.CurrentVolume.text(),
                'xml.Body.GetVolumeResponse.CurrentVolume')

    updateStatus(m.xml.Body.GetTransportInfoResponse.CurrentTransportState.text(),
                 'xml.Body.GetTransportInfoResponse')

    updateMute(m.xml.Body.GetMuteResponse.CurrentMute.text(),
               'xml.Body.GetMute.CurrentMute')
  }
}

def parseServices(device){
  if(device){
    def udn = device?.UDN?.text()
    ["id": udn,
     "name": device?.friendlyName.text(),
     "services": device?.serviceList?.service?.list()?.collect {
       [
         "type": it.serviceType.text(),
         "scdp": it.SCPDURL.text(),
         "control": it.controlURL.text(),
         "events": it.eventSubURL.text()
       ]
     }
    ]
  }
}

// Parse and process subscription updates
def parseEvents(node){
  if(node.size()){
    def xml = parseXml(node.text())
    updateStatus(xml.InstanceID.TransportState.'@val'.text(),
                 'xml.property.LastChange.InstanceID.TransportState')

    updateLevel(xml.InstanceID.Volume.find{it.'@Channel' == 'Master'}.'@val'.text(),
                'xml.property.LastChange.InstanceID.Volume')

    updateMute(xml.InstanceID.Mute.find{it.'@Channel' == 'Master'}.'@val'.text(),
               'xml.property.LastChange.InstanceID.Mute')
  }
}


def parseSid(headers){
  def sid = ""
  if (headers["SID"]){
    sid = headers["SID"]
    sid -= "uuid:"
    sid = sid.trim()
    sid
  }
}

def subscribe() {
  def d = state.services
  def av = d?.services?.find { it.type?.contains('AVTransport') }
  def render = d?.services?.find { it.type?.contains('RenderingControl') }
  def result = []

  if(av?.events){
    result << subscribeAction(av.events)
    result << delayAction(10000)
  }
  if(render?.events){
    result << subscribeAction(render.events)
  }
  result
}

def services(){
  log.debug("GET " + getHostAddress() + path)
  log.debug(state.services)
  new physicalgraph.device.HubAction(
    method: "GET",
    path: path,
    headers: [HOST:getHostAddress()]
  )
}

def soap(action, body, type="AVTransport"){
  log.debug("calling soap: " + action)
  def d = state.services
  def service = d?.services?.find { it.type?.contains(type) }
  if(service){
    new physicalgraph.device.HubSoapAction(
      path:    service.control,
      urn:     service.type,
      action:  action,
      body:    body,
      headers: [Host:getHostAddress(), CONNECTION: "close"])
  }
}

private Integer convertHexToInt(hex) {
  Integer.parseInt(hex,16)
}

private String convertHexToIP(hex) {
  [convertHexToInt(hex[0..1]),convertHexToInt(hex[2..3]),convertHexToInt(hex[4..5]),convertHexToInt(hex[6..7])].join(".")
}

private getHostAddress() {
  def parts = device.deviceNetworkId.split(":")
  def ip = convertHexToIP(parts[0])
  def port = convertHexToInt(parts[1])
  return ip + ":" + port
}

private statusText(s) {
  switch(s) {
  case "PLAYING":
  return "playing"
  case "PAUSED_PLAYBACK":
  return "paused"
  case "STOPPED":
  return "stopped"
  default:
  return s
  }
}

private subscribeAction(path, callbackPath="") {
  log.debug "subscribe($path, $callbackPath)"
  def address = getCallBackAddress()
  def ip = getHostAddress()

  def result = new physicalgraph.device.HubAction(
    method: "SUBSCRIBE",
    path: path,
    headers: [
      HOST: ip,
      CALLBACK: "<http://${address}/notify$callbackPath>",
      NT: "upnp:event",
      TIMEOUT: "Second-28800"])

  result
}

private delayAction(long time) {
  new physicalgraph.device.HubAction("delay $time")
}

private getCallBackAddress(){
  device.hub.getDataValue("localIP") + ":" + device.hub.getDataValue("localSrvPortTCP")
}

private updateSid(sid) {
  if (sid) {
    def sid0 = device.getDataValue('subscriptionId')
    def sid1 = device.getDataValue('subscriptionId1')
    def sidNumber = device.getDataValue('sidNumber') ?: "0"

    log.debug "updateSid($sid), sid0=$sid0, sid1=$sid1, sidNumber=$sidNumber"
    if (sidNumber == "0") {
      if (sid != sid0) {
        updateDataValue("subscriptionId", sid)
        updateDataValue("sidNumber", "1")
      }
    }
    else {
      if (sid != sid1) {
        updateDataValue("subscriptionId1", sid)
        updateDataValue("sidNumber", "0")
      }
    }
  }
}

private updateLevel(current, source){
  if (current) {
    log.debug("Got volume '$current' from '$source'")
    sendEvent(name: "level", value: current, data: [source: source])
  }
}

private updateMute(current, source){
  if (current) {
    log.debug("Got mute '$current' from '$source'")
    def value = currentMute == "1" ? "muted" : "unmuted"
    sendEvent(name: "mute", value: value, descriptionText: "$device.displayName is $value", data: [source: source])
  }
}

private updateStatus(current, source){
  current = statusText(current)
  if (current && current != "TRANSITIONING") {
    log.debug("Got status '$current' from '$source'")
    updateDataValue('currentStatus', current)
    sendEvent(name: "status", value: current, data: [source: source])
    sendEvent(name: "switch", value: current == "playing" ? "on" : "off", displayed: false)
  }
}
